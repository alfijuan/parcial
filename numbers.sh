#!/bin/bash

if [ $# != 2 ]; then

    echo "Error en la cantidad de parametros"

else

    if (($1 == $2)); then

        echo "Son iguales"

    else

        echo "No son iguales"

        if (( ${1} > ${2} )); then

            echo "${1} es mayor que ${2}"

        else

           echo "${1} es menor que ${2}"

        fi

    fi

fi

exit 0
