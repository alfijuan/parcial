#!/bin/bash

#function delete_files_if_equal(){
	if [ $# == 2 ]; then
		if [ ! -e $1 ]; then
			echo -e "$1 no existe"
			exit 1
		fi

		if [ ! -e $2 ]; then
			echo -e "$2 no existe"
			exit 1
		fi

		if cmp -s "$1" "$2"; then
			echo "Los archivos son iguales se borrara $2..."
			rm -f $2
			if [ $? -eq 0 ]; then
				echo "El archivo $2 fue eliminado"
			else
				echo "Ha ocurrido un error al intentar borrar el archivo $2. Verifique los permisos del mismo"
			fi
		else
			echo "files are not same"
		fi

	else
		echo "Para utilizarlo es necesario 2 archivos.";
	fi
#}
