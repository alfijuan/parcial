## Restful API
#### Users
| HTTP          |  Ruta                          | Descripción                                                                      |
| ------------- |--------------------------------| ---------------------------------------------------------------------------------|
| `GET`         | `/users`                       | Obtener listado de usuarios                                                      |
| `GET`         | `/users/{userId}`              | Obtener datos de un usuario                                                      |
| `POST`        | `/users`                       | Crear un usuario                                                                 |
| `PUT`         | `/users/{userId}`              | Modificar datos de un usuario                                                    |
| `DELETE`      | `/users/{userId}`              | Eliminar un usuario                                                              |

#### Benefits
| HTTP          |  Ruta                          | Descripción                                                                      |
| ------------- |--------------------------------| ---------------------------------------------------------------------------------|
| `GET`         | `/benefits`                     | Obtener listado de beneficios                                                        |
| `GET`         | `/benefits/{benefitId}`          | Obtener datos de un beneficio                                                        |
| `POST`        | `/benefits`                     | Crear un beneficio                                                                   |
| `PUT`         | `/benefits/{benefitId}`          | Modificar datos de un beneficio                                                      |
| `DELETE`      | `/benefits/{benefitId}`          | Eliminar un beneficio                                                                |

#### Redeem
Para utilizar este endpoint es necesario usar el token de autentificacion

| HTTP          |  Ruta                          | Descripción                                                                      |
| ------------- |--------------------------------| ---------------------------------------------------------------------------------|
| `GET`         | `/redeems`                  | Obtener listado de canjes                                                    |
| `GET`         | `/redeems/{redeemId}`     | Obtener datos de una canje                                                   |
| `POST`        | `/redeems`                  | Crear un canje                                                              |
| `DELETE`      | `/redeems/{redeemId}`     | Eliminar una canje                                                           |